package P3_Functionari;

/*
* 1.5.3. Intr-un birou sunt 8 functionari care din când în când tipăresc la imprimantă documente,
*  nu toți elaborează documentele în același ritm. Fiindcă au o singură imprimantă în birou, poate tipări doar
*  o singura persoană la un moment dat. Să se simuleze functionarea biroului.
*/

public class Main {
    public void execute(){
        Document document=new Document();
        Imprimanta imprimanta= new Imprimanta(document);
        Functionar f1= new Functionar(1000,1,document);
        Functionar f2= new Functionar(1500,2,document);
        Functionar f3= new Functionar(2000,3,document);
        Functionar f4= new Functionar(500,4,document);
        Functionar f5= new Functionar(1700,5,document);
        Functionar f6= new Functionar(2100,6,document);
        Functionar f7= new Functionar(2500,7,document);
        Functionar f8= new Functionar(900,8,document);


       /* Functionar f1= new Functionar(1000,1,document);
        Functionar f2= new Functionar(1000,2,document);
        Functionar f3= new Functionar(1000,3,document);
        Functionar f4= new Functionar(1000,4,document);
        Functionar f5= new Functionar(1000,5,document);
        Functionar f6= new Functionar(1000,6,document);
        Functionar f7= new Functionar(1000,7,document);
        Functionar f8= new Functionar(1000,8,document);*/

        System.out.println("Incepe printarea!");
        f1.start();
        f2.start();
        f3.start();
        f4.start();
        f5.start();
        f6.start();
        f7.start();
        f8.start();
        imprimanta.start();
    }
}
