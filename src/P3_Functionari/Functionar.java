package P3_Functionari;

public class Functionar extends Thread{

    private int durata;
    private int nrFunctionar;
    //private Imprimanta imprimanta;
    private Document document;

    public Functionar(int durata,int nr, Document document){
        this.durata=durata;
        nrFunctionar=nr;
        //imprimanta=imp;
        this.document=document;
    }

    public void run() {
        while(true){
            try {
                sleep(durata);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Comanda printare de la functionarul "+this.nrFunctionar);
            this.document.pregatirePrintare("Functionarul "+this.nrFunctionar+ " printeaza "+durata+" milisecunde.",nrFunctionar);
            //this.imprimanta.printeaza(new String("Functionarul "+this.nrFunctionar+ " printeaza "+durata+" milisecunde."), durata);

        }
    }
}
