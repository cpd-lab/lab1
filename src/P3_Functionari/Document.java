package P3_Functionari;

public class Document {
    private boolean imprimantaOcupata;
    private String mesaj;

    public Document(){
        imprimantaOcupata=false;
        mesaj="imprimanta initializata";
    }

    public synchronized void pregatirePrintare(String mesajPrintare, int nrFunctionar){
        while (!imprimantaOcupata){
            try {
                System.out.println("Se astepta pentru pregatirea documentelor de printare ale functionarului "+ nrFunctionar);
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.mesaj=mesajPrintare;
        System.out.println("Documentele sunt pregatite pentru printare!");
        imprimantaOcupata=false;
        notifyAll();
    }


    public synchronized void printeaza(){
        while (imprimantaOcupata ){
            try {
                System.out.println("Se astepta pentru printare!");
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(this.mesaj);
        notifyAll();
        this.imprimantaOcupata=true;
    }


}
