package P4_MemorieComuna;

public class Cititor extends Thread{
    private int directie;//1- citirea se face de la stanga la dreapta ; 2- citirea se face de la dreapta la stanga
    private int index;//pozitia caracterului din string la care a ramas citirea
    private String text;
    private int id;

    public Cititor(int id, String string, int directie){
        this.id=id;
        this.text=string;
        this.directie=directie;
        if(directie==1)
            index=0;
        else if(directie==2)
            index= string.length()-1;
        else
            System.out.println("Thread cititor definit incorect! Directia poate ave valori doar din multimea {1,2}");

    }

    @Override
    public void run() {
        if(this.directie==1)
        {
            while(index<this.text.length()) {
               /* try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                System.out.println("Threadul " + this.id + " a citit caracterul: " + this.text.charAt(index++));
            }
        }
        else{
            while(index>=0) {
              /*  try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                System.out.println("Threadul " + this.id + " a citit caracterul: " + this.text.charAt(index--));
            }
        }
    }
}
