package P4_MemorieComuna;
/*1.5.4. Scrieţi un program Java care generează două fire de execuţie pentru parcurgerea unui String de la cele două capete.
*Folosiţi doi pointeri a căror valoare se incrementează, respectiv se decrementează într-o
* funcţie din memoria comună (în memoria comună se află String-ul.)
*/
public class MainP4 {

    public void execute() {
        String text = "123456789abcdefghi";
        Cititor stangaDreapta = new Cititor(1, text, 1);
        Cititor dreaptaStanga = new Cititor(2, text, 2);
        stangaDreapta.start();
        dreaptaStanga.start();
    }
}
